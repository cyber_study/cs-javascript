function layThongTinTuForm() {
  // lấy thông tin từ user
  var maSp = document.getElementById("txtMaSP").value.trim();
  var tenSp = document.getElementById("txtTenSP").value.trim();
  var img = document.getElementById("txtImgSp").value.trim();
  var price = document.getElementById("txtPrice").value.trim();
  var frontCamera = document.getElementById("frontCam").value.trim();
  var backCamera = document.getElementById("backCam").value.trim();
  var screen = document.getElementById("screen").value.trim();
  var desc = document.getElementById("desc").value.trim();
  var type = document.getElementById("type").value.trim();

  // tạo object từ thông tin lấy từ form
  let sp = new CartItem(
    maSp,
    img,
    tenSp,
    price,
    frontCamera,
    backCamera,
    screen,
    desc,
    type
  );
  return sp;
}
