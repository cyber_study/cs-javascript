let emptyCheck = (value, idErr) => {
  if (value == "" || value == "0") {
    document.getElementById(`${idErr}`).style.display = "block";
    document.getElementById(`${idErr}`).innerText = "Không để trống";
    return false;
  } else {
    document.getElementById(`${idErr}`).style.display = "none";
    return true;
  }
};

function cloneCheck(idSp, listSp, idError) {
  var index = listSp.findIndex(function (sp) {
    return sp.id == idSp;
  });

  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã sản phẩm đã tồn tại";
    return false;
  }
}

let isImage = (url) => {
  return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
};

let isUrlImg = (url, idErr) => {
  let isUrlValid = isImage(url);
  if (!isUrlValid) {
    document.getElementById(`${idErr}`).style.display = "block";
    document.getElementById(`${idErr}`).innerText = "URL không phải dạng ảnh";
    return false;
  } else {
    document.getElementById(`${idErr}`).style.display = "none";
    return true;
  }
};

function choseCheck() {
  let choseValue = document.getElementById("type").value;
  if (choseCheck == 0) {
    return false;
  }
  return true;
}
