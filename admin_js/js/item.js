class CartItem {
  constructor(
    _id,
    _img,
    _name,
    _price,
    _frontCamera,
    _backCamera,
    _screen,
    _desc,
    _type
  ) {
    this.id = _id;
    this.img = _img;
    this.name = _name;
    this.price = _price;
    this.frontCamera = _frontCamera;
    this.backCamera = _backCamera;
    this.screen = _screen;
    this.desc = _desc;
    this.type = _type;
  }
}
