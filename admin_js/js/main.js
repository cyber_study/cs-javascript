const BASE_URL = "https://633ec04483f50e9ba3b75fb4.mockapi.io/";

var loadingOn = function () {
  document.getElementById("loading").style.display = "flex";
};
var loadingOff = function () {
  document.getElementById("loading").style.display = "none";
};
// lay danh sach san pham
var fecthDssp = function () {
  loadingOn();
  axios({
    url: `${BASE_URL}/js_capstone`,
    method: "GET",
  })
    .then(function (response) {
      // console.log("error: ", response.data);
      dssp = response.data;
      renderDssp(dssp);
      loadingOff();
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

fecthDssp();

// render danh sach san pham
let renderDssp = (listSv) => {
  var contentHTML = "";
  listSv.forEach(function (sp) {
    contentHTML += `
      <tr>
        <td>${sp.id}</td>
        <td>${sp.name}</td>
        <td class="tdIMG"><img src="${sp.img}" alt="" /></td>
        <td>${sp.price}</td>
        <td>
          <button onclick="suaSp('${sp.id}')" class="btn btn-primary">Sửa</button>
          <button onclick="xoaSp('${sp.id}')" class="btn btn-danger">Xóa</button>
        </td>
      </tr>
    `;
  });

  document.getElementById("itemlist").innerHTML = contentHTML;
};

// xoa san pham
var xoaSp = (idSp) => {
  loadingOn();
  axios({
    url: `${BASE_URL}/js_capstone/${idSp}`,
    method: "DELETE",
  })
    .then(function (res) {
      fecthDssp();
      loadingOff();
      Swal.fire("Xóa thành công!");
      console.log("res :", res);
    })
    .catch(function (err) {
      Swal.fire("Xóa khum thành công!");
      console.log("err: ", err);
    });
  // console.log("idSp :", idSp);
};

// them san pham
var themSp = function () {
  var newSp = layThongTinTuForm();
  console.log(newSp.backCamera);
  console.log(newSp.frontCamera);

  var isValid = true;

  isValid &=
    emptyCheck(newSp.id, "spanMaSP") && cloneCheck(newSp.id, dssp, "spanMaSP");

  isValid &= emptyCheck(newSp.name, "spanTenSP");
  isValid &=
    emptyCheck(newSp.img, "spanImgSp") && isUrlImg(newSp.img, "spanImgSp");
  isValid &= emptyCheck(newSp.frontCamera, "spanFrontCam");
  isValid &= emptyCheck(newSp.backCamera, "spanBackCam");
  isValid &= emptyCheck(newSp.screen, "spanScreen");
  isValid &= emptyCheck(newSp.desc, "spanDesc");
  isValid &= choseCheck(newSp.type, "spanType");
  isValid &= emptyCheck(newSp.price, "spanPrice");

  if (!isValid) return;

  axios({
    url: `${BASE_URL}/js_capstone`,
    method: "POST",
    data: newSp,
  })
    .then(function (res) {
      fecthDssp();
      // console.log("res :", res);
    })
    .catch(function (err) {
      console.log("error :", err);
    });
};

// sua thong tin san pham
var suaSp = function (idSp) {
  axios
    .get(`${BASE_URL}/js_capstone/${idSp}`)
    .then(function (res) {
      document.getElementById("txtMaSP").value = res.data.id;
      document.getElementById("txtMaSP").disabled = true;

      document.getElementById("txtTenSP").value = res.data.name;

      document.getElementById("txtImgSp").value = res.data.img;

      document.getElementById("txtPrice").value = res.data.price;

      document.getElementById("frontCam").value = res.data.frontCamera;

      document.getElementById("backCam").value = res.data.backCamera;

      document.getElementById("screen").value = res.data.screen;

      document.getElementById("desc").value = res.data.desc;

      document.getElementById("type").value = res.data.type;
    })
    .catch(function (err) {
      console.log("err :", err);
    });
};

// reset form
var resetForm = function () {
  document.getElementById("formQLSV").reset();
};

// cap nhat thong tin san pham
var capNhat = function () {
  loadingOn();
  var sp = layThongTinTuForm();
  console.log(sp.backCamera);
  console.log(sp.frontCamera);
  axios({
    url: `${BASE_URL}/js_capstone/${sp.id}`,
    method: "PUT",
    data: sp,
  }).then(function () {
    // console.log(res.data);
    document.getElementById("txtMaSP").disabled = false;
    resetForm();
    loadingOff();
    fecthDssp();
    renderDssp(dssp);
    Swal.fire("Cập nhật thành công!");
  });
};
