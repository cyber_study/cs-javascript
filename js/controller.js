// render danh sách sản phẩm
let renderDSSP = (listSp) => {
  let contentHTML = "";
  listSp.forEach(function (sp) {
    contentHTML += `
      <div class="item">
        <div class="item_container">
          <div class="item-header">
            <p class="price">$${sp.price}</p>
            <span>In Stock</span>
          </div>
          <div class="item-body">
            <img
              src="${sp.img}"
              alt=""
            />
          </div>
          <div class="item-lable">
            <h3>${sp.name}</h3>
          </div>
          <div class="item-desc">
            <p>screen: ${sp.screen}</p>
            <br />
            <p>backCamera: ${sp.backCamera}</p>
            <br />
            <p>frontCamera: ${sp.frontCamera}</p>
            <p class="desc">"${sp.desc}"</p>          
            
            <div id="PlusMinus" class="toggleBtn${sp.id}">
              <button class="updateCart" onclick="minusBtn('${sp.id}')" id="minus">-</button>
              <p class="numberPlace" id="soLuongSP${sp.id}">1</p>
              <button class="updateCart" onclick="plusBtn('${sp.id}')" id="plus">+</button>
           </div>
            
            <button id="suaSvBtn" onclick="addToCartList('${sp.id}')" class="btn btn-buy btn${sp.id} updateCart">ADD</button>
          </div>
        </div>
      </div>
    `;
    // console.log(sp);
  });

  document.getElementById("items_list").innerHTML = contentHTML;
};

// RenderCartList
let renderCartList = (listCartItem) => {
  let contentHTMLCart = "";
  listCartItem.forEach(function (sp) {
    contentHTMLCart += `
      <tr>
        <td>
          <img
            src="${sp.img}"
            alt=""
          />
        </td>
        <td><h3>${sp.name}</h3></td>
        <td>
          <div>
            <button class="updateCart"
              onclick="minusBtn('${sp.id}')"
              id="CartMinus"
            >
             -
            </button>
              <p class="numberPlace" id="soLuongSPCart${sp.id}">${sp.quantity}</p>
            <button
              class="updateCart"
              onclick="plusBtn('${sp.id}')"
              id="CartPlus"
            >
              +
            </button>
          </div>
        </td>
        <td><h3>$${sp.price}</h3></td>
        <td><i class="fa fa-trash" onclick="xoaSp('${sp.id}')"></i></td>
      </tr>
    `;
    // console.log(sp);
  });

  document.getElementById("tbodycartUI").innerHTML = contentHTMLCart;
};
