const BASE_URL = "https://633ec04483f50e9ba3b75fb4.mockapi.io/";
// const axios = require("axios").default;

// import Swal from "sweetalert2";

const CARTLIST = "CARTLIST";
let CartList = [];

let dataJson = localStorage.getItem(CARTLIST);
// console.log(dataJson);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  // console.log("dataRaw :", dataRaw);
  CartList = dataRaw.map((item) => {
    return new CartItem(
      item.id,
      item.img,
      item.name,
      item.quantity,
      item.price
    );
  });

  // console.log(CartList);
}

let fetchDssp = function () {
  axios({
    url: `${BASE_URL}/js_capstone`,
    method: "GET",
  })
    .then(function (res) {
      dssp = res.data;
      // console.log(dssp);
      renderDSSP(dssp);

      dssp.map((sp) => {
        let index = CartList.findIndex(function (item) {
          return item.id == sp.id;
        });

        if (index != -1) {
          document.querySelector(`.btn${sp.id}`).style.display = "none";
          document.querySelector(`.toggleBtn${sp.id}`).style.display = "block";
          document.getElementById(`soLuongSP${sp.id}`).innerText =
            CartList[index].quantity * 1;
        }
      });
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

fetchDssp();

let saveLocalStorage = () => {
  var CartListJson = JSON.stringify(CartList);
  localStorage.setItem(CARTLIST, CartListJson);
};

let filterByType = () => {
  axios({
    url: `${BASE_URL}/js_capstone`,
    method: "GET",
  })
    .then(function (res) {
      dssp = res.data;
      let typeValue = document.getElementById("filterByType").value;
      let contentHTML = "";
      dssp.forEach((sp) => {
        if (typeValue == sp.type) {
          var contentItem = `
            <div class="item">
              <div class="item_container">
                <div class="item-header">
                  <p class="price">$${sp.price}</p>
                  <span>In Stock</span>
                </div>
                <div class="item-body">
                  <img
                    src="${sp.img}"
                    alt=""
                  />
                </div>
                <div class="item-lable">
                  <h3>${sp.name}</h3>
                </div>
                <div class="item-desc">
                  <p>screen: ${sp.screen}</p>
                  <br />
                  <p>backCamera: ${sp.backCamera}</p>
                  <br />
                  <p>frontCamera: ${sp.frontCamera}</p>
                  <p class="desc">"${sp.desc}"</p>

                  <button class="btn btn-buy">ADD</button>
                </div>
              </div>
            </div>
          `;
          contentHTML = contentHTML + contentItem;
        }
      });
      document.getElementById("items_list").innerHTML = contentHTML;
      if (typeValue == "0") {
        renderDSSP(dssp);
      }
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

let addToCartList = (idItem) => {
  // console.log(idItem);
  axios.get(`${BASE_URL}/js_capstone/${idItem}`).then((res) => {
    let sp = new CartItem(
      res.data.id,
      res.data.img,
      res.data.name,
      1,
      res.data.price
    );
    // console.log("Cart Item :", res.data.name, sp);
    // kiem duyet

    document.querySelector(`.btn${idItem}`).style.display = "none";
    document.querySelector(`.toggleBtn${idItem}`).style.display = "block";

    CartList.push(sp);
    updateSoLuongTrongCart();
    saveLocalStorage();
    renderCartList(CartList);
    emptyCheck();
    tinhTien(CartList);
  });
};

document.getElementById("cartToggle").onclick = () => {
  console.log(1);

  let isDisplay = document.getElementById("CartUI").style.display;
  console.log(isDisplay);
  if (isDisplay == "" || isDisplay == "none") {
    renderCartList(CartList);
    document.getElementById("CartUI").style.display = "block";
    document.getElementById("cart").style.display = "none";
    document.getElementById("soLuongTrongCart").style.display = "none";
    document.getElementById("times").style.display = "block";
    let btnCartPlusMinusList = document.querySelectorAll("#CartPlusMinus");
    let btnCartPlusMinusArray = [...btnCartPlusMinusList];
    btnCartPlusMinusArray.forEach((btn) => {
      btn.style.display = "block";
    });
  } else {
    document.getElementById("CartUI").style.display = "none";
    document.getElementById("cart").style.display = "block";
    document.getElementById("soLuongTrongCart").style.display = "flex";
    document.getElementById("times").style.display = "none";
    // document.getElementById("CartPlusMinus").style.display = "none";
  }
};

// minusBtn.onclick = function () {};
let minusBtn = (idSp) => {
  let index = CartList.findIndex(function (item) {
    return item.id == idSp;
  });

  let number = CartList[index].quantity * 1;
  number -= 1;
  if (number == 0) number = 1;

  CartList[index].quantity = number;
  // console.log(CartList[index].name, "co", number);

  document.getElementById(`soLuongSP${idSp}`).innerText = number;
  renderCartList(CartList);
  saveLocalStorage();
  updateSoLuongTrongCart();
  tinhTien(CartList);
};

let plusBtn = (idSp) => {
  let index = CartList.findIndex(function (item) {
    return item.id == idSp;
  });

  let number = CartList[index].quantity * 1;
  number += 1;
  CartList[index].quantity = number;
  // console.log(CartList[index].name, "co", number);

  document.getElementById(`soLuongSP${idSp}`).innerText = number;
  renderCartList(CartList);
  saveLocalStorage();
  updateSoLuongTrongCart();
  tinhTien(CartList);
};

let updateSoLuongTrongCart = () => {
  let soLuongTrongCart = 0;
  for (let i = 0; i < CartList.length; i++) {
    soLuongTrongCart += CartList[i].quantity * 1;
  }
  document.getElementById("soLuongTrongCart").innerText = soLuongTrongCart;
  return soLuongTrongCart;
};

// Open/Close Cart UI
document.getElementById("cartToggle").onclick = () => {
  // console.log(1);

  let isDisplay = document.getElementById("CartUI").style.display;
  // console.log(isDisplay);
  if (isDisplay == "" || isDisplay == "none") {
    renderCartList(CartList);
    document.getElementById("CartUI").style.display = "block";
    document.getElementById("cart").style.display = "none";
    document.getElementById("soLuongTrongCart").style.display = "none";
    document.getElementById("times").style.display = "block";
    let btnCartPlusMinusList = document.querySelectorAll("#CartPlusMinus");
    let btnCartPlusMinusArray = [...btnCartPlusMinusList];
    btnCartPlusMinusArray.forEach((btn) => {
      btn.style.display = "block";
    });
  } else {
    document.getElementById("CartUI").style.display = "none";
    document.getElementById("cart").style.display = "block";
    document.getElementById("soLuongTrongCart").style.display = "flex";
    document.getElementById("times").style.display = "none";
    // document.getElementById("CartPlusMinus").style.display = "none";
  }
};

let xoaSp = (idSp) => {
  let index = CartList.findIndex(function (sp) {
    return sp.id == idSp;
  });

  if (index == -1) return;

  document.querySelector(`.btn${idSp}`).style.display = "block";
  document.querySelector(`.toggleBtn${idSp}`).style.display = "none";

  CartList.splice(index, 1);
  updateSoLuongTrongCart();
  renderCartList(CartList);
  saveLocalStorage();
  emptyCheck();
  tinhTien(CartList);
};

let emptyCheck = () => {
  if (updateSoLuongTrongCart() == 0) {
    document.getElementById("emptyToggle").style.display = "block";
    return true;
  } else {
    document.getElementById("emptyToggle").style.display = "none";
    return false;
  }
};

emptyCheck();

function clearCartList() {
  console.log(1);
  CartList = [];
  updateSoLuongTrongCart();
  tinhTien(CartList);
  saveLocalStorage();
  renderDSSP(dssp);
  renderCartList(CartList);
}

function purchase() {
  if (!tinhTien(CartList) == "") {
    alert("Đơn hàng của bạn đã được đặt thành công");
  } else {
    alert("Chưa có hàng trong giỏ đồ");
  }
}

let tinhTien = (list) => {
  if (emptyCheck()) {
    document.getElementById("totalMoney").innerText = `$0`;
    return;
  }
  let tongTien = 0;
  for (let i = 0; i < list.length; i++) {
    tongTien += list[i].quantity * 1 * list[i].price * 1;
  }

  document.getElementById("totalMoney").innerText = `$${tongTien}`;
  return tongTien;
  // console.log(tongTien);
};

tinhTien(CartList);
