class CartItem {
  constructor(_id, _img, _name, _quantity, _price) {
    this.id = _id;
    this.img = _img;
    this.name = _name;
    this.quantity = _quantity;
    this.price = _price;
  }
}
